require('dotenv').config();

const express = require('express'),
    app = express(),
    port = process.env.PORT,
    Sequelize = require('sequelize'),
    { Client } = require('pg'),
    bodyParser = require('body-parser');

const sequelize = new Sequelize(process.env.DATABASE_URL);

// const client = new Client({ connectionString: process.env.DATABASE_URL });
// client.connect();

// const sequelize = new Sequelize({
//     dialect: 'sqlite',
//     storage: 'path/to/database.sqlite'
// });

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const employeeRoute = require('./src/api/routes/employee.route');
app.use('/', employeeRoute);

app.listen(port);
console.log(`Administration started on: ${port}`)

