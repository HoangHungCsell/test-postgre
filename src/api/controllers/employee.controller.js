const User = require('../models/employee.model');
const client = require('../../../config');

exports.create = async (req, res) => {
    const user = await User.create(req.body);
    return res.json(user);
};

exports.list = (req, res) => {
    User.findAndCountAll().then(users => {
        // console.log("All users:", JSON.stringify(users, null, 4));
        return res.json({ users })
    });
};

exports.update = async (req, res) => {
    const { name, favoriteColor, age, cash } = req.body;
    await User.update({
        name,
        favoriteColor,
        age,
        cash
    }, {
        where: {
            id: req.params.id
        }
    });
    const user = User.findOne({
        where: { id: req.params.id }
    });
    return res.json({
        message: `Đã cập nhật user, id: ${req.params.id}`,
        user
    });
};

exports.delete = async (req, res) => {
    const user = User.findOne({
        where: { id: req.params.id }
    });
    await User.destroy({ where: { id: req.params.id } });
    return res.json({
        message: `Đã xóa user, id: ${req.params.id}`,
        user
    })
};

exports.detail = async (req, res) => {
    const user = await User.findOne({
        where: { id: req.params.id }
    });
    return res.json(user);
};