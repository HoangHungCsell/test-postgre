require('dotenv').config();

const { Sequelize, Model, DataTypes } = require("sequelize");
const sequelize = new Sequelize(process.env.DATABASE_URL);

const User = sequelize.define("User", {
    name: {
        type: DataTypes.TEXT,
        unique: true
    },
    favoriteColor: {
        type: DataTypes.TEXT,
        defaultValue: 'green'
    },
    age: DataTypes.INTEGER,
    cash: DataTypes.INTEGER
}, {
    tableName: 'users'
});

// (async () => {
//     await sequelize.sync({ force: true });
//     // Code here
// })();

module.exports = User;