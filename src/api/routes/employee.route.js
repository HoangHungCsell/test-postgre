
const express = require('express');
const controller = require('../controllers/employee.controller');
const router = express.Router();

router
    .route('/employees')
    .get(
        controller.list
    )
    .post(
        controller.create
    );

router
    .route('/employees/:id')
    .get(
        controller.detail
    )
    .put(
        controller.update
    )
    .delete(
        controller.delete
    );

module.exports = router;